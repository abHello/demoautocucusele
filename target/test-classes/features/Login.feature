Feature: Login, Logout, failed login

  Scenario: Login with valid informations
    Given I'm on the login page
    When I login with valid informations
    Then login succed

  Scenario: Login with invalid username
    Given I'm on the login page
    When I login with invalid username
    Then login failed because of username

  Scenario: Login with invalid password
    Given I'm on the login page
    When I login with invalid password
    Then login failed because of password

  Scenario: Logout
    Given I'm on the login page
    When I login with valid informations
    When I logout
    Then logout succed
