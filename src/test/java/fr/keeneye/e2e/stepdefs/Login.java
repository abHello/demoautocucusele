package fr.keeneye.e2e.stepdefs;


import fr.keeneye.e2e.components.LoginPage;
import fr.keeneye.e2e.helpers.PropertiesHelper;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;


public class Login {
    WebDriver driver;
    PropertiesHelper prop;
    LoginPage loginpage=new LoginPage();

    public Login(){
        this.driver=Hooks.driver;
        this.prop=Hooks.lp;
    }

    @Given("I'm on the login page$")
    public void getLoginPage(){
        driver.get(prop.getProperty("app.url"));
    }

    @When("I login with valid informations$")
    public void validLogin(){
        loginpage.login(prop.getProperty("app.username.valid"),prop.getProperty("app.password.valid"));
    }

    @When("I login with invalid username")
    public void invalidLoginUsername(){
        loginpage.login(prop.getProperty("app.username.invalid"),prop.getProperty("app.password.valid"));
    }

    @When("I login with invalid password$")
    public void invalidLoginPassword(){
        loginpage.login(prop.getProperty("app.username.valid"),prop.getProperty("app.password.invalid"));
    }

    @When("I logout$")
    public void logout(){
        loginpage.logout();
    }

    @Then("login succed$")
    public void loginSucces(){
        Assert.assertEquals("Expected alert not found.",prop.getProperty("login.succes.alert"),loginpage.getFlashMessage());
    }

    @Then("login failed because of username")
    public void loginFailedUsername(){
        Assert.assertEquals("Invalid username.",prop.getProperty("invalid.usename.alert"),loginpage.getFlashMessage());
    }

    @Then("login failed because of password$")
    public void loginFailedPassword(){
        Assert.assertEquals("Invalid Password.",prop.getProperty("invalid.password.alert"),loginpage.getFlashMessage());
    }
    @Then("logout succed$")
    public void logoutSucces(){
        Assert.assertEquals("Expected alert for logout not found.",prop.getProperty("logout.succes.alert"),loginpage.getFlashMessage());
    }
}
