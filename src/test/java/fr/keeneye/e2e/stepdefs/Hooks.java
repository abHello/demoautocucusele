package fr.keeneye.e2e.stepdefs;

import fr.keeneye.e2e.helpers.PropertiesHelper;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Hooks {

    private  String browser,type;
    public static WebDriver driver;
    public static PropertiesHelper lp;

    @Before
    public void DriverStart(){

        /** Load data */
        lp=new PropertiesHelper();
        lp.loadProperties("src/test/resources/data");
        type=lp.getProperty("type");
        browser=lp.getProperty("browser");

        if(type.equals("local")){
            switch (browser){
                case "chrome":
                    System.setProperty("webdriver.chrome.driver",lp.getProperty("chrome.driver.location"));
                    driver=new ChromeDriver();
                    break;

                case "safari":
                    System.setProperty("webdriver.safari.driver",lp.getProperty("safari.driver.location"));
                    driver=new SafariDriver();
                    break;
        }}else if(type.equals("remote")){
            DesiredCapabilities capabilities = new DesiredCapabilities();
            switch (browser){
                case "chrome":
                    capabilities.setCapability("browserName", "chrome");
                case "safari":
                    capabilities.setCapability("browserName", "safari");
            }

            try {
                driver = new RemoteWebDriver(new URL("https://"+"IP"+"Port"+"/wd/hub"), capabilities);
            } catch (MalformedURLException e) {
                System.out.println("Invalid URL");
            }
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void teardown() throws InterruptedException {

        Thread.sleep(3000);
        driver.quit();
    }

}
