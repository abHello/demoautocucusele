package fr.keeneye.e2e.stepdefs;

import fr.keeneye.e2e.components.JqueryMenusPage;
import fr.keeneye.e2e.helpers.PropertiesHelper;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;



public class JqueryMenus {
    WebDriver driver;
    PropertiesHelper prop;
    JqueryMenusPage jq=new JqueryMenusPage();

    public JqueryMenus(){
        this.driver=Hooks.driver;
        this.prop=Hooks.lp;
    }

    @Given("I'm on jquery page$")
    public void getJqueryPage(){
        driver.get(prop.getProperty("app.url.jquery"));
    }

    @When("I click on menu (.*) then on item (.*)$")
    public void clickMenu(String menu, String item){
        jq.clickMenu(menu, item);
    }

    @When("I'm on new page$")
    public void getOnNewPage(){
        Assert.assertEquals("Expected title not found.",prop.getProperty("h3.text"),jq.getH3Page());
    }


}
