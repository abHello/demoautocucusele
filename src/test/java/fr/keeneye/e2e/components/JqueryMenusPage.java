package fr.keeneye.e2e.components;

import fr.keeneye.e2e.stepdefs.Hooks;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class JqueryMenusPage {
    WebDriver driver;

    public JqueryMenusPage(){
        this.driver= Hooks.driver;
    }

    public void clickMenu(String menuToClick, String itemToClick){
        List<WebElement> menus=driver.findElement(By.cssSelector("#menu")).findElements(By.tagName("li"));
        for(WebElement menu:menus){
            if(menu.getText().equals(menuToClick)){
                new Actions(driver).moveToElement(menu).perform();
                WebElement menuItem = driver.findElement(By.xpath("//*[text()='"+itemToClick+"']"));
                menuItem.click();
                break;
            }
        }
    }

    public String getH3Page(){
        return driver.findElement(By.tagName("h3")).getText();
    }
}
