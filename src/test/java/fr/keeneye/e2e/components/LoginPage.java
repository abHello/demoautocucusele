package fr.keeneye.e2e.components;

import fr.keeneye.e2e.stepdefs.Hooks;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    WebDriver driver;

    public LoginPage(){
        this.driver= Hooks.driver;
    }

    public void login(String username, String password){
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector(".radius")).click();
    }

    public String getFlashMessage(){
        return driver.findElement(By.cssSelector("#flash")).getText();
    }

    public void logout(){
        driver.findElement(By.cssSelector(".button")).click();
    }
}
