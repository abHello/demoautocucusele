package fr.keeneye.e2e.helpers;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.logging.Logger;


public class PropertiesHelper {
    private static Properties properties;
    private static Logger logger = Logger.getGlobal();

    public void loadProperties(String dataFolder){
        try {
            properties = new Properties();

            // Load data properties files
            File folder = new File(dataFolder);
            File[] listOfFiles = folder.listFiles();

            for (File file : listOfFiles) {
                if (file.isFile()) {
                    properties.load(new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8)));
                }
            }

        } catch (IOException e) {
            logger.info("An error has occured when trying to load the properties file from profile");
        }

    }

    public  String getProperty(String key) {
        return properties.getProperty(key);
    }

}
